﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BulkSpellCheck;
using System.IO;

namespace BulkSpellCheckTests
{
    [TestClass]
    public class SpellCheckerTest
    {

        [TestMethod]
        public void TestAddFileFormat()
        {

            SpellChecker spellChecker = new SpellChecker();
            IFileFormat powerpointFileFormat = new PowerpointFileFormat();

            Assert.AreEqual(spellChecker.FormatsToCheck.Count, 0);
            Assert.IsFalse(spellChecker.FormatsToCheck.Contains(powerpointFileFormat));

            spellChecker.AddFileFormat(powerpointFileFormat);
            Assert.AreEqual(spellChecker.FormatsToCheck.Count, 1);
            Assert.IsTrue(spellChecker.FormatsToCheck.Contains(powerpointFileFormat));

        }

        [TestMethod]
        public void TestRemoveFileFormat()
        {

            SpellChecker spellChecker = new SpellChecker();
            IFileFormat powerpointFileFormat = new PowerpointFileFormat();

            spellChecker.AddFileFormat(powerpointFileFormat);
            Assert.AreEqual(spellChecker.FormatsToCheck.Count, 1);
            Assert.IsTrue(spellChecker.FormatsToCheck.Contains(powerpointFileFormat));

            spellChecker.RemoveFileFormat(powerpointFileFormat);
            Assert.AreEqual(spellChecker.FormatsToCheck.Count, 0);
            Assert.IsFalse(spellChecker.FormatsToCheck.Contains(powerpointFileFormat));

        }

        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void TestInvalidFilePath()
        {

            SpellChecker spellChecker = new SpellChecker();
            IFileFormat powerpointFileFormat = new PowerpointFileFormat();
            spellChecker.AddFileFormat(powerpointFileFormat);

            spellChecker.TargetPath = "ABC:\\\\ThisShouldNotWork\\";
            spellChecker.Run();

        }

        [TestMethod]
        public void TestNoFileFormats()
        {

            SpellChecker spellChecker = new SpellChecker();

            spellChecker.TargetPath = "C:\\";
            var result = spellChecker.Run();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(SpellCheckDirectoryResult));
            
        }

        [TestMethod]
        public void TestPowerpoint()
        {

            SpellChecker spellChecker = new SpellChecker();
            spellChecker.TargetPath = "C:\\";
            spellChecker.AddFileFormat(new PowerpointFileFormat());

            var result = spellChecker.Run();
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(SpellCheckDirectoryResult));

        }

    }
}
