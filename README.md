# Bulk Spell Checker

## What is it?

A .NET library that will spell check a directory of files and return the results

## What's next?

- Refactor to filter on extension, rather than checking each file manually
- Complete test coverage
- Add new file types
  - The other OpenXML formats
  - .story
  - ???