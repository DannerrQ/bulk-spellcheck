﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class SpellChecker
    {

        public string TargetPath { get; set; }
        public List<IFileFormat> FormatsToCheck { get; }

        public SpellChecker()
        {
            FormatsToCheck = new List<IFileFormat>();
        }

        public void AddFileFormat(IFileFormat target)
        {
            if (!FormatsToCheck.Contains(target))
            {
                FormatsToCheck.Add(target);
            }
        }

        public void RemoveFileFormat(IFileFormat target)
        {
            FormatsToCheck.Remove(target);
        }
        
        public SpellCheckDirectoryResult Run()
        {

            // First check the target path is OK
            if (!Directory.Exists(TargetPath))
            {
                throw new DirectoryNotFoundException();
            }

            int resultsCount = 0;
            List<SpellCheckFileResult> results = new List<SpellCheckFileResult>();
            DirectoryInfo targetDirectory = new DirectoryInfo(TargetPath);
            foreach (var targetFile in targetDirectory.GetFiles())
            {
                foreach (var format in FormatsToCheck)
                {

                    if (!format.IsFileThisFormat(targetFile))
                    {
                        continue;
                    }

                    using (var targetSpellCheckFile = format.LoadFile(targetFile))
                    {
                        SpellCheckFileResult spellCheckFileResult = targetSpellCheckFile.SpellCheck();
                        resultsCount += spellCheckFileResult.Results.Count();
                        results.Add(targetSpellCheckFile.SpellCheck());
                    }

                    // break the foreach format
                    break;

                }

            }

            return new SpellCheckDirectoryResult(targetDirectory.GetFiles().Length, resultsCount, results);

        }

    }
}
