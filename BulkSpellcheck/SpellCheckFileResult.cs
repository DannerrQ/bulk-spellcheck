﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class SpellCheckFileResult
    {

        public string Name { get; }
        public List<SpellCheckResult> Results { get; }

        public SpellCheckFileResult(string name, List<SpellCheckResult> results)
        {
            Name = name;
            Results = results;
        }

    }
}
