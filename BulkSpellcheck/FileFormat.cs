﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class FileFormat
    {

        public static FileFormat Text = new FileFormat("Text", ".txt");
        public static FileFormat Powerpoint = new FileFormat("Powerpoint", ".pptx");

        public string Name { get; set; }
        public string Extension { get; set; }
        
        private FileFormat(string name, string extension)
        {
            Name = name;
            Extension = extension;
        }

    }
}
