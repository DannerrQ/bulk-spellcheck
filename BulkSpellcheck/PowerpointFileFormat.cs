﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class PowerpointFileFormat : IFileFormat
    {
        public bool IsFileThisFormat(FileInfo target)
        {
            return target.Extension.ToUpper() == ".PPTX";
        }

        public ISpellCheckFile LoadFile(FileInfo target)
        {
            var spellCheckFile = new PowerpointSpellCheckFile();
            spellCheckFile.Load(target);

            return spellCheckFile;
        }
    }
}
