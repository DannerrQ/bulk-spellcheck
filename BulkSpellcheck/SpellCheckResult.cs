﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class SpellCheckResult
    {
        
        public string Location { get; }
        public string Word { get; }
        public List<string> Suggestions { get; }

        public SpellCheckResult(string location, string word, IEnumerable<string> suggestions)
        {
            Location = location;
            Word = word;
            Suggestions = new List<string>();

            foreach (var suggestion in suggestions)
            {
                Suggestions.Add(suggestion);
            }
        }

    }
}
