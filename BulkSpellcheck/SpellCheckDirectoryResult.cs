﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public class SpellCheckDirectoryResult
    {

        public int FileCount { get; }
        public int ResultCount { get; }
        public List<SpellCheckFileResult> FileResults { get; }

        public SpellCheckDirectoryResult(int fileCount, int resultCount, List<SpellCheckFileResult> fileResults)
        {
            FileCount = fileCount;
            ResultCount = resultCount;
            FileResults = fileResults;
        }

    }
}
