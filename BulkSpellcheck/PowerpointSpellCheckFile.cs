﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using OpenXmlDrawing = DocumentFormat.OpenXml.Drawing;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace BulkSpellCheck
{
    class PowerpointSpellCheckFile : ISpellCheckFile
    {

        private FileInfo fileInfo;
        private PresentationDocument presDoc;

        public void Load(FileInfo target)
        {
            fileInfo = target;
            presDoc = PresentationDocument.Open(fileInfo.FullName, false);
        }

        public SpellCheckFileResult SpellCheck()
        {

            List<SpellCheckResult> results = new List<SpellCheckResult>();

            var thread = new Thread(() =>
            {

                int numSlides = GetSlideCount();
                var checkerTextBox = new System.Windows.Controls.TextBox();
                checkerTextBox.SpellCheck.IsEnabled = true;

                for (var i = 0; i < numSlides; i++)
                {
                
                    string slideContent = GetSlideContent(i);
                    checkerTextBox.Text = slideContent;

                    int errorIndex = checkerTextBox.GetNextSpellingErrorCharacterIndex(0, System.Windows.Documents.LogicalDirection.Forward);
                    while (errorIndex > -1)
                    {
                        var spellingError = checkerTextBox.GetSpellingError(errorIndex);
                        var incorrectWord = slideContent.Substring(errorIndex, checkerTextBox.GetSpellingErrorLength(errorIndex));

                        results.Add(new SpellCheckResult("Slide " + (i + 1) + " Content", incorrectWord, spellingError.Suggestions));

                        errorIndex += incorrectWord.Length;
                        errorIndex = checkerTextBox.GetNextSpellingErrorCharacterIndex(errorIndex, System.Windows.Documents.LogicalDirection.Forward);
                    }

                    // Same again for notes
                    string slideNotes = GetSlideNotes(i);
                    checkerTextBox.Text = slideNotes;

                    errorIndex = checkerTextBox.GetNextSpellingErrorCharacterIndex(0, System.Windows.Documents.LogicalDirection.Forward);
                    while (errorIndex > -1)
                    {
                        var spellingError = checkerTextBox.GetSpellingError(errorIndex);
                        var incorrectWord = slideNotes.Substring(errorIndex, checkerTextBox.GetSpellingErrorLength(errorIndex));

                        results.Add(new SpellCheckResult("Slide " + (i + 1) + " Notes", incorrectWord, spellingError.Suggestions));

                        errorIndex += incorrectWord.Length;
                        errorIndex = checkerTextBox.GetNextSpellingErrorCharacterIndex(errorIndex, System.Windows.Documents.LogicalDirection.Forward);
                    }

                }

            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            return new SpellCheckFileResult(fileInfo.Name, results);

        }

        private int GetSlideCount()
        {
            int slideCount = 0;

            if (presDoc.PresentationPart != null)
            {
                slideCount = presDoc.PresentationPart.SlideParts.Count();
            }

            return slideCount;

        }

        private SlidePart GetSlide(int index)
        {
            
            // Basically just get the ID based on index then get that part
            var slideIds = presDoc.PresentationPart.Presentation.SlideIdList.ChildElements;
            return (SlidePart) presDoc.PresentationPart.GetPartById((slideIds[index] as SlideId).RelationshipId);

        }

        private string GetSlideContent(int index)
        {

            SlidePart target = GetSlide(index);
            StringBuilder targetContent = new StringBuilder();

            // Get every bit of text on the slide and put in a string
            var texts = target.Slide.Descendants<OpenXmlDrawing.Text>();
            foreach (var text in texts)
            {
                targetContent.AppendLine(text.Text);
            }

            return targetContent.ToString();

        }

        private string GetSlideNotes(int index)
        {

            SlidePart target = GetSlide(index);
            NotesSlidePart targetNotes = target.NotesSlidePart;

            if (targetNotes == null)
            {
                return "";
            }

            StringBuilder targetContent = new StringBuilder();

            // Get every bit of text on the slide and put in a string
            var texts = targetNotes.NotesSlide.Descendants<OpenXmlDrawing.Text>();
            foreach (var text in texts)
            {
                targetContent.AppendLine(text.Text);
            }

            return targetContent.ToString();

        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    presDoc.Dispose();
                }
                
                disposedValue = true;
            }
        }
        
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
