﻿using System;
using System.IO;

namespace BulkSpellCheck
{
    public interface ISpellCheckFile : IDisposable
    {
        void Load(FileInfo target);
        SpellCheckFileResult SpellCheck();
    }
}
