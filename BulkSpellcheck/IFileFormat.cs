﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSpellCheck
{
    public interface IFileFormat
    {
        bool IsFileThisFormat(FileInfo target);
        ISpellCheckFile LoadFile(FileInfo target);
    }
}
